const toNumber = require("./toNumber");

describe("toNumber", () => {
  test("1 => 1", () => {
    expect(toNumber(1)).toBe(1);
  });

  test("\"1\" => 1", () => {
    expect(toNumber("1")).toBe(1);
  });

  test("\"a\" is error", () => {
    expect(() => {
      toNumber("a");
    }).toThrow("value 'a' is not a number!");
  });

  test("Number 97 in ascii is \"a\"", () => {
    expect(toNumber(97)).toBe("a");
  });

  test("1 => 1", () => {
    expect(toNumber(1)).not.toBe("1");
  });
});
